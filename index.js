function createNewUser() {
    const newUser = {
        firstName: prompt('Введите имя'),
        lastName: prompt('Введите фамилию'),
        getLogin: () => (this.firstName[0] + this.lastName).toLowerCase(),
        getAge: () => {
            birthDay = new Date(prompt("Введите дату: 'yyyy.mm.dd'"))
            diff = (new Date().getTime()) - birthDay.getTime(),
            days = Math.floor((diff / (1000 * 60 * 60 * 24)) % 30.44);
            months = Math.floor((diff / (1000 * 60 * 60 * 24 * 30.44)) % 12);
            years = Math.floor(diff / (1000 * 60 * 60 * 24 * 365.25));
            return (`Вы прожили: ${years} лет ${months} месяца ${days} дней`);
        },
        getPassword: () => newUser.firstName[0].toUpperCase() + newUser.lastName.toLowerCase() + this.birthDay.getFullYear(),
    }
    return newUser;
}

const user = createNewUser();
console.log(user);
console.log(user.getAge());
console.log(user.getPassword());